<?php

use App\Modules\Content\Models\Menu as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('key',128)->nullable();
            $table->string('title',255)->nullable();
            $table->string('url',150)->nullable()->unique();
            $table->boolean('active')->default(true);
            $table->integer('order')->unsigned()->default(1);
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('parent_id', model::table() . '_parent_id_fk')
                ->references('id')->on(model::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
