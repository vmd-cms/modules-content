<?php
namespace VmdCms\Modules\Content\database\seeds;

use App\Modules\Content\Sections\MenuFooter;
use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;
use App\Modules\Content\Sections\Page;
use App\Modules\Content\Sections\MenuHeader;

class ModuleSeeder extends AbstractModuleSeeder
{

    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            Page::class => "Pages",
            MenuHeader::class => "Menu Header",
            MenuFooter::class => "Menu Footer"
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
          [
              "slug" => "content_group",
              "title" => "Контент",
              "is_group_title" => true,
              "order" => 3,
              "children" => [
                  [
                      "slug" => "menu_header",
                      "title" => "Меню Header",
                      "icon" => "icon icon-menu-burger",
                      "section_class" => MenuHeader::class
                  ],                  [
                      "slug" => "menu_footer",
                      "title" => "Меню Footer",
                      "icon" => "icon icon-menu-burger",
                      "section_class" => MenuFooter::class
                  ],
                  [
                      "slug" => "pages",
                      "title" => "Инфостраницы",
                      "section_class" => Page::class,
                      "icon" => "icon icon-copy",
                  ]
              ]
          ]
        ];
    }

    protected function seedModelData()
    {
        $arr = [
            [
                'title' => 'Бренды',
                'url' => 'brands',
                'active' => true,
                'children' => []
            ],
            [
                'title' => 'О нас',
                'url' => 'about',
                'active' => true,
                'children' => []
            ],
            [
                'title' => 'Доставка и оплата',
                'url' => 'delivery',
                'active' => true,
                'children' => []
            ],
            [
                'title' => 'Гарантия/Возврат',
                'url' => 'guarantee',
                'active' => true,
                'children' => []
            ],
            [
                'title' => 'Блог',
                'url' => 'blog',
                'active' => true,
                'children' => []
            ],
            [
                'title' => 'Контакты',
                'url' => 'contacts',
                'active' => true,
                'children' => []
            ]
        ];

        foreach ($arr as $item)
        {
            $this->recursiveSeed($item,null);
        }
    }

    protected function recursiveSeed($item, $parent_id)
    {
        $model = new \App\Modules\Content\Models\MenuHeader();
        $model->parent_id = $parent_id;
        $model->active = $item['active'];
        $model->url = $item['url'];
        $model->save();
        $modelInfo = new \App\Modules\Content\Models\MenuInfo();;
        $modelInfo->menu_id = $model->id;
        $modelInfo->title = $item['title'];
        $modelInfo->url = $item['url'];
        $modelInfo->save();
        if(count($item['children']))
        {
            foreach ($item['children'] as $child) $this->recursiveSeed($child, $model->id);
        }
        return;
    }

}
