<?php

namespace VmdCms\Modules\Content\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class MenuInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'menu_info';
    }
}
