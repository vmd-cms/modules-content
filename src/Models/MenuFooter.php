<?php

namespace VmdCms\Modules\Content\Models;

class MenuFooter extends Menu
{
    const MODEL_KEY = 'footer';

    public static function getModelKey(){
        return self::MODEL_KEY;
    }
}
