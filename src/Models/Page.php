<?php

namespace VmdCms\Modules\Content\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\SeoableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Models\Seoable;

class Page extends CmsModel implements ActivableInterface, HasInfoInterface, OrderableInterface, SeoableInterface
{
    use Activable, HasInfo, Orderable, Seoable;

    public static function table(): string
    {
       return 'pages';
    }
}
