<?php

namespace VmdCms\Modules\Content\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class PageInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'pages_info';
    }
}
