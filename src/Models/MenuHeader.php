<?php

namespace VmdCms\Modules\Content\Models;

class MenuHeader extends Menu
{
    const MODEL_KEY = 'header';

    public static function getModelKey(){
        return self::MODEL_KEY;
    }
}
