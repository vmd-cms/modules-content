<?php

namespace VmdCms\Modules\Content\Initializers;;

use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'content';
    const ALIAS = 'Content';

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }
}
