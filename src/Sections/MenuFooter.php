<?php

namespace VmdCms\Modules\Content\Sections;

class MenuFooter extends Menu
{
    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\MenuFooter::class;
    }
}
