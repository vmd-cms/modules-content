<?php

namespace VmdCms\Modules\Content\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterDisplay;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterTreeComponent;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

class Menu extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'menu';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Menu";
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        $filter = (new FilterDisplay())
            ->appendFilterComponent((new FilterTreeComponent('info.title'))
                ->setHeadTitle($this->getTitle())
                ->setModelClass($this->getCmsModelClass())
            );
        return Display::dataTable([
            ColumnEditable::text('info.title','Название')->maxLength(10),
            ColumnEditable::text('info.url','URL')->unique(),
            ColumnEditable::switch('active', 'Состояние')->alignCenter(),
        ])->setSearchable(true)->setFilter($filter)->orderable();
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('info.title','Название')->required(),
            FormComponent::input('info.url','URL')->required()->unique(),
            FormComponent::select('parent_id','Группа')
                ->setModelForOptions($this->getCmsModelClass())
                ->setDisplayField('info.title')
                ->setForeignField('id')
                ->setWhere(['parent_id','=',null])
                ->nullable(),
            FormComponent::radio('active','Состояние')
        ])->setHeadTitle('Редактирование пункта меню: ', 'info.title');
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\Menu::class;
    }
}
