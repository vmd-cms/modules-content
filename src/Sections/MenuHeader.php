<?php

namespace VmdCms\Modules\Content\Sections;

class MenuHeader extends Menu
{
    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\MenuHeader::class;
    }
}
