<?php

use Illuminate\Support\Facades\Route;

Route::get('/content', function (){
    return view("content::template")->render();
});
